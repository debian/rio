# os2.mak
# Created by IBM WorkFrame/2 MakeMake at 21:04:53 on 24 Mar 1999
#
# The actions included in this make file are:
#  Compile::C++ Compiler
#  Link::Linker

.SUFFIXES: .CPP .obj

.all: rio.exe

.CPP.obj:
    icc.exe /Sp1 /Ss /Q /G5 /Tm /Fo"%|dpfF.obj" /C %s

rio.exe: APP.obj RIO.obj io.obj {$(LIB)}os2.def
    icc.exe @<<
     /B" /st:65535 /nologo"
     /Ferio.exe
     io.obj
     os2.def
     APP.obj
     RIO.obj
<<

APP.obj: \
    APP.CPP \
    {;$(INCLUDE);}std.h \
    {;$(INCLUDE);}rio.h

RIO.obj: \
    RIO.CPP \
    {;$(INCLUDE);}std.h \
    {;$(INCLUDE);}binary.h \
    {;$(INCLUDE);}rio.h \
    iopl32.h
