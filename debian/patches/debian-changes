This is an autogenerated patch header for a single-debian-patch file. The
delta against upstream is either kept as a single patch, or maintained
in some VCS, and exported as a single patch instead of more manageable
atomic patches.

--- rio-1.07.orig/app.cpp
+++ rio-1.07/app.cpp
@@ -43,7 +43,7 @@
 	#endif
 	#define		DELETEARRAY				delete[]
 
-#elif defined(__FreeBSD__)
+#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
 	// FreeBSD g++
 	#include	<unistd.h>
 	#include	<sys/syslimits.h>
--- rio-1.07.orig/makefile
+++ rio-1.07/makefile
@@ -6,11 +6,19 @@
 #
 ###############################################################################
 
+# Editied for Debian GNU/Linux
+DESTDIR 	=
+BIN		= $(DESTDIR)/usr/bin
+
+
 all: 		rio
 
 rio:		app.cpp rio.cpp
-		g++ -O1 -o rio app.cpp rio.cpp
-		chmod +s rio
+		g++ -O1 -o rio app.cpp rio.cpp $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
+#		chmod +s rio
+
+install:	rio
+		install ./rio $(BIN)
 
 clean:
 		rm -f rio
--- /dev/null
+++ rio-1.07/rio.1
@@ -0,0 +1,116 @@
+.TH RIO 1 
+.\" NAME should be all caps, SECTION should be 1-8, maybe w/ subsection
+.\" other parms are allowed: see man(7), man(1)
+.SH NAME
+rio \- program to control a Diamond RIO MP3 player
+.SH SYNOPSIS
+.B rio
+.I "[options] files ..."
+.br
+.SH DESCRIPTION
+This manual page documents briefly the
+.BR rio 
+command.
+This manual page was written for the Debian GNU/Linux distribution
+because the original program does not have a manual page.
+.PP
+.B rio
+is a CLI controller for a Diamond Rio MP3 file player.
+.PP
+The default ioport is 0x378.  If this is not the ioport for your 
+printer port, you will have to use the
+.B -p
+option.
+.SH INITIALIZATION
+.PP
+Initialization should only be performed on new or corrupt cards.
+You can initialize with bad block checking enabled (-iy) which
+will test all blocks on the internal or external flash ram (which
+ever is selected) and note all bad blocks. This process will
+take some time (upto 20 minutes on slow machines).
+You can also initialize with bad block checking disabled (-in)
+which is much quicker but all blocks (even bad blocks) will
+be used.
+.PP
+If you want to delete all files on the device, the (-za) option
+should be used instead of using the initialization options
+mentioned above.
+.PP
+.SH OPTIONS
+A summary of options are included below.
+.TP
+.B \-d
+Display Directory
+.TP
+.B \-iy
+initialize with check for bad blocks
+.TP
+.B \-in
+initialize without check for bad blocks
+.TP
+.B \-x
+perform operations on external flash ram
+.TP
+.B \-u
+specify file(s) to upload
+.TP
+.B \-g
+specify file to download
+.TP
+.B \-f
+specify text based playlist file which contains files to be upload
+.TP
+.B \-z
+specify file to delete
+.TP
+.B \-za
+delete all files
+.TP
+.B \-o
+specify new playlist order in quotes
+.TP
+.B \-p
+specify parallel port base IO address, default=0x378
+.TP
+.B \-v
+enable verbose mode
+.TP
+.B \-di
+specify initialization delay
+.TP
+.B \-dt
+specify tx delay
+.TP
+.B \-dr
+specify rx delay
+.SH EXAMPLES
+.TP
+display directory using parallel port at 0x278
+.PP
+rio -p 0x278 -d
+.TP
+initialize (with bad block check) and upload files
+.PP
+rio -iy -u *.mp3
+.TP
+delete existing files and upload playlist onto external flash ram
+.PP
+rio -za -f playlist.txt -x
+.TP
+initialize, upload files in playlist and then display directory
+.PP
+rio -d -in -f playlist.txt
+.TP
+download file then delete it and finally display directory
+.PP
+rio -d -g mp3Files/song.mp3 -z song.mp3
+.TP
+reverse playlist order and display directory, also change rx iodelay
+.PP
+rio -o "5 4 3 2 1" -d -dr 4
+.SH SEE ALSO
+file:/usr/doc/rio/README.gz
+http://www.world.co.uk.sba/index.htm
+.SH AUTHOR
+This manual page was written by Vince Mulhollon <vlm@debian.org>,
+for the Debian GNU/Linux system (but may be used by others).
--- rio-1.07.orig/rio.cpp
+++ rio-1.07/rio.cpp
@@ -63,18 +63,36 @@
 #elif defined(__linux__)
 	// linux g++
 	#include	<unistd.h>
-	#if defined(__alpha)
-		#include	<sys/io.h>
+
+	// Changed per Debian bug 50938
+	//#if defined(__alpha)
+	//	#include	<sys/io.h>
+	//#else
+	//	#include	<sys/perm.h>
+	//#endif
+
+	// Changes per Debian bug 50938
+	#if !defined(__i386__)
+		#include        <sys/io.h>
 	#else
-		#include	<sys/perm.h>
+		#include        <sys/perm.h>
 	#endif
+	
+	// Changed per bug Debian bug 88779
+	#if !defined(__alpha__)
+	#if __GLIBC__
+	#include	<sys/io.h>
+	#else
 	#include	<asm/io.h>
+	#endif
+	#endif
+
 	#define		OUTPORT(p,v)			outb( v, p )
 	#define		INPORT(p)				inb( p )
 	#define		CLOCK_SECOND			CLOCKS_PER_SEC
 	#define		DELETEARRAY				delete[]
 
-#elif defined(__FreeBSD__)
+#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
 	// FreeBSD g++
 	#include	<fcntl.h>
 	#include	<unistd.h>
@@ -236,7 +254,7 @@ BOOL CRio::Set( int iPortBase )
 	#endif
 
 	// if FreeBSD
-	#if defined(__FreeBSD__)
+	#if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
 		// request direct access to memory addresses
 		if ( open("/dev/io", O_RDONLY) == -1 )
 		{
--- rio-1.07.orig/std.h
+++ rio-1.07/std.h
@@ -31,7 +31,7 @@ typedef char CBOOL;
 #endif
 
 #ifndef		SZERROR
-#define		SZERROR						sys_errlist[errno]
+#define		SZERROR						strerror(errno)
 #endif
 
 typedef unsigned char UCHAR;
