#ifdef __cplusplus
extern "C" {
#endif

USHORT _Far16 _Pascal RPORT(USHORT);
void _Far16 _Pascal WPORT(USHORT, USHORT);

#ifdef __cplusplus
}
#endif
